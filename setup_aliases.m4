readonly LAST_MODIFY='2020.08.04'
readonly AUTHOR='Petar Toshev'
readonly VERSION='1.0'
readonly PROGRAM_NAME='Setup alias'
readonly DESCRIPTION="
        ${PROGRAM_NAME} is script helping copying aliases
        Version: ${VERSION}
        Created by ${AUTHOR}
        Last modification: ${LAST_MODIFY}
"
#########################################################################
## Parameters
#########################################################################

# ARG_HELP([${DESCRIPTION}])
# ARG_VERSION([echo ${PROGRAM_NAME} ${VERSION}])
#
# ARG_OPTIONAL_BOOLEAN([git], [g], [Add git aliases])
# ARG_OPTIONAL_BOOLEAN([server], [s], [Add server aliases])
# ARG_OPTIONAL_BOOLEAN([npm], [n], [Add npm and yarn aliases])
# ARG_OPTIONAL_BOOLEAN([docker], [d], [Add docker aliases])
# ARG_OPTIONAL_BOOLEAN([prompt], [p], [Change PS1/PROMPT variable])
#
# ARGBASH_GO

# [ <-- needed because of Argbash
#########################################################################
# Variables
#########################################################################
readonly ADD_VARIABLES='add_variables';

export CURRENT_FILE=$(readlink -f "$0") 
export CURRENT_DIR=$(dirname "$CURRENT_FILE") 
export PARENT_DIR=$(dirname "$CURRENT_DIR") 
readonly SHELL_NAME="$(ps -hp $$ | awk '{print $5}')"
readonly SHELL_DIR="${HOME}/.${SHELL_NAME}"
mkdir -p "$SHELL_DIR"
. "${CURRENT_DIR}/colors"
if [ "${SHELL_NAME}" == 'zsh' ]; then . "${CURRENT_DIR}/colors-zsh"; fi

#########################################################################
# Functions
#########################################################################
put() {
  ALIASES_FILENAME="${SHELL_DIR}/${1}"

  QUERY="if [ -f \"${ALIASES_FILENAME}\" ]; then . \"${ALIASES_FILENAME}\"; fi"
  SHRC="${HOME}/.${SHELL_NAME}rc"

  QUERY_ESC=$(printf '%s' "$QUERY" | sed 's/\([]\[]\)/\\\1/g')

  # Check if and add ${HOME}/.bashrc to use new file as source1 
  if ! grep -q "${QUERY_ESC}" "${SHRC}"; then echo "${QUERY}" >> "${SHRC}"; fi

  FILE_TO_COPY="${CURRENT_DIR}/files/${1}"
  # Change new file
  if [[ "$2" == "${ADD_VARIABLES}" ]]; then
    envsubst "${ALL_COLORS_VARIABLES::-1},\${CURRENT_FILE},\${CURRENT_DIR},\${PARENT_DIR}" < "${FILE_TO_COPY}" > "${ALIASES_FILENAME}"
  elif [[ ! -f "${ALIASES_FILENAME}" ]]; then
    ln -s "${FILE_TO_COPY}" "${ALIASES_FILENAME}"
  fi
}

var_expand() {
  eval printf '%s' "\"\${_arg_${1}}\""
}

add_file() {
  if [[ "$(var_expand $1)" == 'on' ]]; then put "$1" "$2"; fi
}


#########################################################################
# Main
#########################################################################
readonly MACHINE_TYPE="$(uname -s)"
case "${MACHINE_TYPE}" in
    Linux*)     put linux
                DISTRO_TYPE=$(cat /etc/*-release | grep ID_LIKE= | sed 's/ID_LIKE=//')
                if [[ "$DISTRO_TYPE" == 'arch' ]]; then put arch; fi;;
    Darwin*)    put mac;;
    *)          ;;
esac

put basic
put functions

add_file git
add_file docker
add_file npm
add_file server
add_file git-status ${ADD_VARIABLES}
add_file prompt ${ADD_VARIABLES}

# ] <-- needed because of Argbash
