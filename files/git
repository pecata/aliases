alias ga='git add'
alias gb='git branch'
alias gba='gb -a'
alias gcm='git commit'
alias gd='git diff'
alias gdc='gd --cached'
alias gf='git fetch'
alias gfo='gf origin'
alias gl='git log --graph --pretty=format:"%C(auto,yellow)%h%x09%C(auto,green)%ad%x09%C(auto,cyan)%<(10,trunc)%an %C(auto,reset)%s%C(auto,red)%d" --date=format:"%a %Y-%m-%d %H:%M:%S" --author-date-order'
alias gla='gl --all'
alias glr='gl --date=relative'
alias glar='glr --all'
alias gm='git merge'
alias gp='git pull'
alias gr='git restore' 
alias grs='gr --staged'
alias gsh='git stash'
alias gshl='gsh list'
alias gshpo='gsh pop'
alias gshpu='gsh push'
alias gst='git status'
alias gsu='git submodule update --recursive --remote'
alias gu='git push'
alias gurb='git remote prune origin' # git-update-remote-branches
alias guab='gurb && gulb' # git-update-all-branches

# git-branch-current
gbc() {
  git branch 2>/dev/null | awk '/^*/ {print $2}'
}

# git-branch-regex
gbr() {
  git branch --all | grep $1 | sed 's/^* //;s/^  //;s/^remotes\/origin\///' | head -n 1
}

# git-checkout
gch() {
  if [ ${#1} -eq 0 ]
  then
    err missing branch name
    return 1
  fi

  RES="$(git checkout $1 2>&1 >/dev/null)"
  if [ $(echo $RES | grep 'pathspec' | wc -l) -gt 0 ]
  then
    git checkout -b $1
  else
    printf "${RES}\n"
  fi
}

# git-checkout-regex
gchr() {
  branch="$(gbr $1)"
  gch "$branch"
}

# git-log-unpushed-branches
glub() {
  # git log --branches --not --remotes --simplify-by-decoration --decorate --oneline
  # echo or maybe second is better
  git for-each-ref --format="%(refname:short) %(push:track)" refs/heads | grep '\[*\]' --color=never
}

# git-log-unpushed-commits
gluc() {
  git log origin/$(gbc)..${1:-HEAD} --oneline
}

# git-merge-regex
gmr() {
  branch="$(gbr $1)"
  git merge $branch
}


# git-update-local-branches
gulb() {
  git branch -vv | grep ': gone]' | awk '/\*/{next}{print $1}' | xargs git branch -d 
}

# git-commit-push
gcp() {
  gcm && gu
}

# git-add-commit-push
gacp() {
  ga "${*:-.}" && gcp
}

# git-add-commit
gac() {
  ga "${*:-.}" && gcm
}
